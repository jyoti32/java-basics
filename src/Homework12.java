import java.util.*;

public class Homework12 {
    public static void main(String[] args) {
       /*
        1. Create a new file Homework12.java file with Main method in IntelliJ IDEA
        2. We had the students from the following countries in the cohort #1 of the bootcamp: United States, United States, Ukraine, Mexico
        3. In the cohort #2 we had the following countries: United States, Canada, United States, Mexico
        4. Print out the list of all unique countries of every cohort separately (order doesn't matter)
        5. Print out all unique countries from all cohorts in one line (order doesn't matter)
        6. Print out all unique countries from all cohorts in alphabetical order
        7. Print out the common countries from both cohorts
        8. Create a new branch and commit your changes
        9. Push your code to a remote repository
        10. Create a pull request
        11. Copy and paste the link of the pull request (or your branch) to the field below, so we can check your homework
        */


        Set<String> cohort1 = new TreeSet<String>();
        cohort1.add("United States");
        cohort1.add("United States");
        cohort1.add("United States");
        cohort1.add("Ukraine");
        cohort1.add("Mexico");

        Set<String> cohort2 = new TreeSet<String>();
        cohort2.add("United States");
        cohort2.add("Canada");
        cohort2.add("United States");
        cohort2.add("Mexico");

        System.out.println("Unique countries from cohort1: " + cohort1);
        System.out.println("Unique countries from cohort2: " + cohort2);
        System.out.println();

        Set<String> allCohort = new TreeSet<String>(cohort1);
        allCohort.addAll(cohort2);
        System.out.println("Unique countries from all cohort: " + allCohort);
        System.out.println();

        Set<String> commonCohort = new HashSet<String>(cohort1);
        commonCohort.retainAll(cohort2);
        System.out.println("Common countries from both cohorts: " + commonCohort);
        System.out.println();


    }
}
