public class Homework10 {
    public static void main(String[] args) {
        BankAccount myBankAccount = new BankAccount();

        myBankAccount.setBalance(500);
        myBankAccount.setBalance(150);
        myBankAccount.setBalance(35);
        myBankAccount.setBalance(-40);
        myBankAccount.setBalance(-120);

        myBankAccount.getBalance();

        myBankAccount.setBalance(-900);
    }
}

class BankAccount {
    private double balance = 0;

    public double getBalance() {
        System.out.println("Your current balance is $"  + this.balance);
        return balance;
    }

    public void setBalance(double depositAmt) {
        double newBalance = this.balance + depositAmt;
        if(newBalance >= 0) {
            this.balance = newBalance;
            System.out.println("Your balance has changed by $" + depositAmt +
                    " and now it is: $" + this.balance);
        } else {
            System.out.println("Withdrawal of $" + (-depositAmt) + " cannot be completed. " +
                    "Your balance is $" + this.balance);
        }
    }
}
