public class Homework9 {
    public static void main(String[] args) {
        LinkElement linkElement = new LinkElement();
        TextElement textElement = new TextElement();

        System.out.println("Link Element: " + linkElement.locator);
        System.out.println("Text Element: " + textElement.locator);
        System.out.println("Both elements have same locator because of inheritance principle");

        System.out.println();
        linkElement.click();
        textElement.click();
        System.out.println("Both elements have different click response because of polymorphism principle");
    }
}

class BaseElement {
    public String locator = "my-locator";

    public void click(){
        System.out.println("clicked on an element");
    }
}

class LinkElement extends BaseElement {
    public void click(){
        System.out.println("clicked and redirected");
    }
}

class TextElement extends BaseElement {

}
