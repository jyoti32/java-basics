import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Homework13 {
    public static void main(String[] args) {
       /*
       1. Create a new file Homework13.java file with Main method in IntelliJ IDEA
       2. Declare a HashMap with Integer values for Street Numbers & String values for Names:
       3. (1000, Liam), (1001, Noah), (1002, Olivia), (1003, Emma), (1004, Benjamin), (1005, Evelyn),  (1006, Lucas)
       4. Find the name of the person who lives at 1004 based on the street number
       5. Next, print out all the odd number streets and its corresponding names
       6. Create a new branch and commit your changes
       7. Push your code to a remote repository
       8. Create a pull request
       9. Copy and paste the link of the pull request (or your branch) to the field below, so we can check your homework
        */

        HashMap<Integer, String> streetNumAndName = new HashMap<Integer, String>();
        streetNumAndName.put(1000, "Liam");
        streetNumAndName.put(1001, "Noah");
        streetNumAndName.put(1002, "Olivia");
        streetNumAndName.put(1003, "Exam");
        streetNumAndName.put(1004, "Benjamin");
        streetNumAndName.put(1005, "Evelyn");
        streetNumAndName.put(1006, "Lucas");

        String name = streetNumAndName.get(1004);
        System.out.println(name + " lived in street " + 1004);
        System.out.println();

        name = streetNumAndName.get(1001);
        System.out.println(name + " lived in street " + 1001);
        name = streetNumAndName.get(1003);
        System.out.println(name + " lived in street " + 1003);
        name = streetNumAndName.get(1005);
        System.out.println(name + " lived in street " + 1005);

    }
}
