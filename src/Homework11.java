import java.util.ArrayList;
import java.util.List;

public class Homework11 {
    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<Integer>();
        list1.add(-2);
        list1.add(-3);
        list1.add(6);
        list1.add(3);

        List<Integer> list2 = new ArrayList<Integer>();
        list2.add(-2);
        list2.add(-4);
        list2.add(5);
        list2.add(-3);

        List<Integer> list3 = new ArrayList<Integer>();
        // list3.addAll(list1); -- alternate way to add all
        for(Integer each : list1) {
            list3.add(each);
        }
        // list3.addAll(list2); -- alternate way to add all
        for(Integer each : list2) {
            list3.add(each);
        }

        Integer min = list3.get(0);
        for(Integer each  : list3) {
            if(min > each) {
                min = each;
            }
        }

        System.out.println("Minimum value in list 3 is: " + min);

    }
}
