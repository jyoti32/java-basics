
import java.util.Scanner;

public class Main {
    public static void main(String[] args2){
        // System.out.println("Hello world again..");
        // demonstrateTryCatchException();
        doCatchExceptionHomework();




    }

    static void doCatchExceptionHomework() {
        try {
            int[] array = {1, 2, 3};
            int fourth = array[3];
        } catch (Exception e) {
            System.out.println("This is why QA Engineers always have" +
                    " to do boundary testing! The array only has 3 values" +
                    " and you've requested a 4th");
        } finally {
            System.out.println("In final block...");
        }

    }


    static void demonstrateTryCatchException() {
        Scanner scan = new Scanner(System.in);

        int i = 0;
        do {
            try{
                System.out.print("Enter first number: ");
                int num1 = scan.nextInt();
                System.out.print("Enter second number: ");
                int num2 = scan.nextInt();

                int result = num1/num2;
                System.out.println();

                System.out.println("Dividing " + num1 + " by " + num2 + " gives " + result + " as quotient.");
                i =1;
            } catch (Exception e) {
                System.out.println();
                System.out.println("You can't do that.Try again!");
                System.out.println();
            } finally {
                System.out.println("I am now in final block");

            }
        } while (i == 0);
    }
}
