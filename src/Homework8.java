public class Homework8 {

    public static void main(String[] args) {

//        - Finish up the logic that calculates the balance
//        - Create a method that withdraws from the balance
//        - Check that if a bank customer deposits $500, $150 and $35 and then withdraws $40 and $120 the balance is correct
//        - Print out a meaningful message if it is correct and if it is not

        BankingAccount myAccount = new BankingAccount();
        myAccount.deposit(500);
        // please finish the rest
        myAccount.deposit(150); // 500 + 150 = 650
        myAccount.deposit(35);  // 650 + 35 = 685
        myAccount.withdraw(40); // 685 - 40 = 645
        myAccount.withdraw(120);    // 645 - 120 = 525

        if(myAccount.balance == 525) {
            System.out.println("Bank balance is correct!");
        } else {
            System.out.println("Bank balance is not correct!");
        }

    }
}

class BankingAccount {

    public int balance;

    public void deposit(int sum) {
        this.balance += sum; // this.balance = this.balance + sum;
    }

    public void withdraw(int amt) {
        this.balance -= amt;
    }
}
